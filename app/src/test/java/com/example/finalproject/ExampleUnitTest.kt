package com.example.finalproject

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.lifecycle.LiveData
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.example.finalproject.Api.ApiModel
import com.google.accompanist.pager.rememberPagerState
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Assert
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import java.io.IOException


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}

class Test {
    @Test
        fun swipeWorks() {
        val pagerState = 1
        assertEquals(1, pagerState)

    }
}

class Test2 {

    @Test
    @Composable
    fun Test2() {
        val vm = ApiModel()
        // arrange
        val size = vm.userList.size

        // act
        val things = 4

        // assert
        assertEquals(things, size)
    }
}

class test3 {
    private lateinit var userDao: DrinkDatabaseDao
    private lateinit var db: DrinkDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, DrinkDatabase::class.java).build()
        userDao = db.drinkDatabaseDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun dataCheck() {
        val drinkIndex = 0

        assertEquals(0 , drinkIndex)
    }


}



