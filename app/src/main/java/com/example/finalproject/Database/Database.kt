package com.example.finalproject


import android.content.Context
import androidx.lifecycle.LiveData

import androidx.room.*


@Suppress("unused")

@Entity(tableName = "DrinkList")
data class Drink(

    @ColumnInfo(name = "DrinkName")
    val drinkName: String,

    @PrimaryKey(autoGenerate = true)
    val id: Int,

    @ColumnInfo(name = "Scale")
    val scaleFav: Int,

    )


//need to edit the Album data class
@Database(entities = [Drink::class], version = 2, exportSchema = false)

abstract class DrinkDatabase : RoomDatabase() {
    abstract fun drinkDatabaseDao(): DrinkDatabaseDao


    companion object {
        @Volatile
        private var INSTANCE: DrinkDatabase? = null
        fun getInstance(context: Context): DrinkDatabase {
            synchronized(this) {

                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        DrinkDatabase::class.java,
                        "DrinkList"
                    )
                        .fallbackToDestructiveMigration()
                        .build()

                }
                return INSTANCE as DrinkDatabase
            }

        }
    }

}
@Dao
interface DrinkDatabaseDao {
    @Query("SELECT drinkName, id, Scale FROM DrinkList")
    fun getDrinks(): LiveData<MutableList<Drink>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(drink: Drink)




}
