package com.example.finalproject

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DrinkViewModel(application: Application): ViewModel() {
    private val db= DrinkDatabase.getInstance(application)


    internal val drinkList: LiveData<MutableList<Drink>> = db.drinkDatabaseDao().getDrinks()

    fun insert(drink: Drink){
        viewModelScope.launch(Dispatchers.IO) {
            db.drinkDatabaseDao().insert(drink)
        }
    }



}

class DrinkViewmodelFactory(private val application: Application): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DrinkViewModel::class.java)) {
            return DrinkViewModel(application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")

    }
}