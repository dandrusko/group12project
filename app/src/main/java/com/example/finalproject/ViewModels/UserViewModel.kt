package com.example.finalproject.ViewModels

import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.example.finalproject.Api.Friends
import kotlinx.coroutines.delay

//keeps track on if the user is signed into their account or not
//logout button or signup button??

class UserViewModel : ViewModel() {

    var loggedIn by mutableStateOf(false)
    var notLoggedIn by mutableStateOf(false)
    var signUp by mutableStateOf(false)
    var Logout by mutableStateOf(false)
    var RecordDrinks by mutableStateOf(false)
    var SelectLocation by mutableStateOf(false)
    var FriendsFav by mutableStateOf(false)
    var Login by mutableStateOf(false)
    var back by mutableStateOf(false)

    suspend fun LogOut() {
        notLoggedIn = true
        loggedIn = false
        signUp = false
        SelectLocation = false
        FriendsFav = false

    }
    suspend fun back() {
        notLoggedIn = false
        loggedIn = true
        signUp = false

    }

    suspend fun signIn(username: String, password: String) {
        notLoggedIn = true
        delay(2000)
        loggedIn = true
        notLoggedIn = false
    }
    suspend fun signUp(username: String, password: String) {
        signUp = true
        delay(2000)
        loggedIn = true
        signUp = false
    }

    suspend fun SelectLocation(){
        SelectLocation = true
        loggedIn = false
        signUp = false

    }


    suspend fun FriendsFav(){
        FriendsFav = true
        SelectLocation = false
        loggedIn = false
        signUp = false

    }
}
//used across all pages
val user = compositionLocalOf<UserViewModel> { error("ERROR") }

