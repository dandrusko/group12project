package com.example.finalproject.navigation

sealed class Screen(val route: String){
    object MainScreen : Screen("MainActivity")
    object RecordDrink : Screen("RecordDrinks")
    object SelectLocations : Screen("SelectLocation")
    object FriendsFavs : Screen("FriendsFav")
    object LoginPages : Screen("LoginPage")

    fun withArgs(vararg args: String): String {
    return buildString {
        append(route)
        args.forEach { arg ->
            append("/$arg")
        }
    }
    }
}