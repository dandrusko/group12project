package com.example.finalproject.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.finalproject.MainScreen
import com.example.finalproject.SelectLocation

@Composable
fun nav(){
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Screen.MainScreen.route){
        composable(route = Screen.MainScreen.route){
        MainScreen()

        }
        composable(
            route = Screen.SelectLocations.route + "/{name}",
            arguments = listOf(
                    navArgument("name"){
                        type = NavType.StringType
                        defaultValue = "Group12"
                        nullable = true
                    }
            )
        )
        { entry ->
            SelectLocation()
        }
    }
}
