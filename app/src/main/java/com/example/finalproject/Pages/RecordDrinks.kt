package com.example.finalproject

import android.app.Application
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme

import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*

import androidx.compose.ui.Alignment

import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.KeyboardType

import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.finalproject.ViewModels.user


@Composable
fun RecordDrinks() {

    var drinkName by remember { mutableStateOf("") }
    var scale by remember { mutableStateOf("") }
    var id by remember { mutableStateOf("") }
    val context = LocalContext.current

    var username by remember { mutableStateOf("") }
    var pwd by remember { mutableStateOf("") }
    val coroutineScope = rememberCoroutineScope()
    val state = user.current


    val model : DrinkViewModel = viewModel(
        factory = DrinkViewmodelFactory(
            context.applicationContext as Application
        )
    )

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(
            "Record a Drink You Like",
            fontSize = 36.sp,
            modifier = Modifier
                .padding(16.dp)

        )

          TextField(value = drinkName,
              onValueChange = { newInput -> drinkName = newInput },
              label = { Text(text = "Drink Name",color = MaterialTheme.colors.background) },
              keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
              modifier = Modifier
                  .padding(top = 25.dp)
                  .background(color = MaterialTheme.colors.primaryVariant)
          )

          TextField(value = id,
              onValueChange = { newInput -> id = newInput },
              label = { Text(text = "Drink ID Number",color = MaterialTheme.colors.background) },
              keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
              modifier = Modifier
                  .padding(top = 25.dp)
                  .background(color = MaterialTheme.colors.primaryVariant)
          )

          TextField(value = scale,
              onValueChange = { newInput -> scale = newInput },
              label = { Text(text = "Love the Drink? 1-10",color = MaterialTheme.colors.background) },
              keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
              modifier = Modifier
                  .padding(top = 25.dp)
                  .background(color = MaterialTheme.colors.primaryVariant)
          )

          Button(

              onClick = {
                  model.insert(
                      Drink(
                          drinkName,
                          id.toInt(),
                          scale.toInt()
                      )
                  )

              }){
              Text(text = "Add Drink")
          }
    }
}



