package com.example.finalproject

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import com.example.finalproject.ViewModels.user
import kotlinx.coroutines.launch


//@Preview(showBackground = true, showSystemUi = true)
@Composable
fun SelectLocation()  {

    val coroutineScope = rememberCoroutineScope()

    val state = user.current

    // has the current state of the drop down box, list of locations
    var expanded by remember {mutableStateOf(false)}
    val locations = listOf("Towson", "Villanova", "College Park", "Tallahassee")
    var selectedItem by remember {mutableStateOf("")}
    var textFilledSize by remember {mutableStateOf(Size.Zero)}
    var isSelected by remember {mutableStateOf(false)}

    // direction of the keyboard arrow if the drop down box is expanded or not
    val icon = if (expanded) {
        Icons.Filled.KeyboardArrowUp
    } else {
        Icons.Filled.KeyboardArrowDown
    }
    
    Column(modifier = Modifier.padding(20.dp)) {
        Spacer(modifier = Modifier.height(32.dp))
        OutlinedTextField(value = selectedItem, onValueChange = {selectedItem = it},
        modifier = Modifier
            .fillMaxWidth()
            .onGloballyPositioned { coordinates ->
                textFilledSize = coordinates.size.toSize()
            },
        label = { Text(text = "Select your location") },
        trailingIcon = {
            Icon(icon, "", Modifier.clickable {expanded = !expanded})
        }
        )

        // drop down menu
        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
                .width(with(LocalDensity.current){textFilledSize.width.toDp()})
        ) {
            locations.forEach {city ->
                DropdownMenuItem(onClick = {
                    selectedItem = city
                    expanded = false
                    isSelected = true
                }) {
                    Text(text = city)
                }
            }
        }

        Button(onClick = {
            // the behavior if a dropdown menu is selected or not when the onclick is triggered
            if (isSelected){
                val fireNotification = CreateNotification(selectedItem)
                fireNotification.createNotification()
            }

        },
            modifier = Modifier
                .padding(20.dp)
                .fillMaxWidth()) {
            Text(text = "Select")
        }
        Button(onClick = {
            coroutineScope.launch {
                state.back()
            }
        }, modifier = Modifier.size(width = 120.dp,height = 80.dp)) {
            Text(text = "MainScreen")
        }
    }

}
