package com.example.finalproject

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.ParagraphStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextIndent
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.finalproject.Api.ApiModel
import com.example.finalproject.ViewModels.user
import kotlinx.coroutines.launch
import kotlin.text.Typography

//this page is the implementation of seeing friedn whom you follow favorite drinks.
@Preview
@Composable
fun FriendsFav() {
    val vm = ApiModel()
    ApiView(vm)
    }//edn freindsfav

@Composable
fun ApiView(vm: ApiModel) {
    val coroutineScope = rememberCoroutineScope()

    val state = user.current


    LaunchedEffect(Unit, block = {
        vm.getFriendList()
    })

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Row {
                        Text("Friends Favorites Drinks")
                    }
                })
        },
        content = {

            if (vm.errorMessage.isEmpty()) {
                Column(modifier = Modifier.padding(16.dp)) {
                    LazyColumn(modifier = Modifier.fillMaxHeight()) {
                        items(vm.userList) { user ->
                            Column {
                                Row(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(16.dp),
                                    horizontalArrangement = Arrangement.SpaceBetween
                                ) {
                                    Box(
                                        modifier = Modifier
                                            .fillMaxWidth()
                                            .padding(0.dp, 0.dp, 16.dp, 0.dp)
                                    ) {
                                        Text(
                                            user.first + " " + user.last + "  Favorite Drink: "+ user.drink,
                                            maxLines = 1,
                                            overflow = TextOverflow.Ellipsis
                                        )

                                    }


                                }
                                Divider()
                            }
                            Button(onClick = {
                                coroutineScope.launch {
                                    state.back()
                                }
                            }, modifier = Modifier.size(width = 120.dp,height = 80.dp)) {
                                Text(text = "MainScreen")
                            }

                        }
                    }
                }
            } else {
                Text(vm.errorMessage)
            }
        }
    )

}