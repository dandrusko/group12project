package com.example.finalproject

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.material.TextField
import kotlinx.coroutines.launch
import androidx.compose.material.Button
import androidx.compose.ui.tooling.preview.Preview
import com.example.finalproject.ViewModels.user
import com.example.finalproject.ViewPager.ViewPager
import com.google.accompanist.pager.ExperimentalPagerApi


//this is the login page
//add a sign up button that tak the user to ap page to sign on
//currently nothing ia validate
//user can typ anything in and be direct to main screen
@OptIn(ExperimentalPagerApi::class)
@Preview
@Composable
fun LoginPage() {
    //inout values we want to remember
    var username by remember { mutableStateOf("") }
    var pwd by remember { mutableStateOf("") }

    //inorder to call the state.sign we need a coroutine
    val coroutineScope = rememberCoroutineScope()
    //remembering the users state : isNotLoggedIN
    val state = user.current
    ViewPager()
    Column(
        Modifier
            .fillMaxSize()
            .padding(32.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        if (state.notLoggedIn) {
            CircularProgressIndicator()
        } else {
            Spacer(modifier = Modifier.height(32.dp))

            Spacer(modifier = Modifier.height(16.dp))
            TextField(value = username,
                onValueChange = { newInput -> username = newInput },
                label = { Text(text = "Username",color = MaterialTheme.colors.background) },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
                modifier = Modifier
                    .padding(top = 25.dp)
                    .background(color = MaterialTheme.colors.primaryVariant)
            )
            TextField(value = pwd,
                onValueChange = { newInput -> pwd = newInput },
                label = { Text(text = "Password",color = MaterialTheme.colors.background) },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
                modifier = Modifier
                    .padding(top = 25.dp)
                    .background(color = MaterialTheme.colors.primaryVariant)
            )
            Spacer(modifier = Modifier.height(16.dp))

            Button(onClick = {
                coroutineScope.launch {
                    state.signIn(username, pwd)
                }
            }) {
                Text(text = "Sign In")
            }
            //this is the sign in button just called register
            //currently will move to main screen until sign up is implemented
            Spacer(modifier = Modifier.height(16.dp))
            Button(onClick = {
                coroutineScope.launch {
                    state.signUp(username, pwd)
                }
            }) {
                Text(text = "Register")
            }

        }//edn else


    }
}


