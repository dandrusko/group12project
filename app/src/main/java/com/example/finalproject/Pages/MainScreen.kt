package com.example.finalproject

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.ParagraphStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextIndent
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.finalproject.ViewModels.user
import com.example.finalproject.navigation.Screen

import kotlinx.coroutines.launch




//screen a user will see after they login
//we can add a logout button??
//need to reformat this and add function to the buttons

@Composable

fun MainScreen() {


    val result = remember { mutableStateOf("") }
    val scaffoldState = rememberScaffoldState(
        rememberDrawerState(DrawerValue.Closed)
    )
    val scope = rememberCoroutineScope()

    val navController = rememberNavController()

    val coroutineScope = rememberCoroutineScope()

    val state = user.current

    var username by remember { mutableStateOf("") }
    var pwd by remember { mutableStateOf("") }
    var text by remember{ mutableStateOf("")}



    NavHost(navController = navController, startDestination = "MainScreen") {
        composable("MainScreen") { MainActivity() }
        composable("RecordDrinks") { RecordDrinks() }


    }

    Scaffold(
        scaffoldState = scaffoldState,
        drawerContent = {
            Column(

                modifier = Modifier
                    .fillMaxHeight()
                    .fillMaxWidth()
                    .padding(5.dp),
                verticalArrangement = Arrangement.Center, Alignment.CenterHorizontally




            ) {

                Spacer(modifier = Modifier.padding(8.dp))

                Button(onClick = {
                    coroutineScope.launch {
                        state.SelectLocation()
                    }

                }, modifier = Modifier.size(width = 120.dp,height = 80.dp)
                ){
                    Text(text = "Select your location")
                }


                Spacer(modifier = Modifier.padding(8.dp))

                Button(onClick = {
                    coroutineScope.launch {
                        state.FriendsFav()
                    }
                }, modifier = Modifier.size(width = 120.dp,height = 80.dp)) {
                    Text(text = "Friends Favorite Drinks")
                }
                Spacer(modifier = Modifier.padding(8.dp))
                Button(onClick = {
                    coroutineScope.launch {
                        state.LogOut()
                    }
                }, modifier = Modifier.size(width = 120.dp,height = 80.dp)
                ){
                    Text(text =  "log out")

                }
            }// end column




        },//end content

        topBar = {
            TopAppBar(
                title = {
                    Text(text = "12")
                },
//hamburger icon to open up the drawer
                navigationIcon = {
                    IconButton(
                        onClick = {
                            scope.launch {
                                scaffoldState.drawerState.open()
                            }
                        }
                    ) {
                        Icon(Icons.Filled.Menu, contentDescription = "")
                    }
                },

                backgroundColor = Color(0xFF5FA777),
                elevation = AppBarDefaults.TopAppBarElevation
            )
        },//edn top bar

        content = {
            Column(
            modifier = Modifier
                .background(Color(0xFFFFFAF0))
                .fillMaxSize()
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(75.dp),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = "Your Saved Drinks",
                    fontSize = 30.sp,
                    fontFamily = FontFamily.Cursive,
                    textAlign = TextAlign.Center
                )


            }

            Column(
                modifier = Modifier
            ) {
                Row(modifier = Modifier
                    .clickable {

                        scope.launch {
                            scaffoldState.drawerState.close()


                        }
                    }
                    .fillMaxWidth()
                    .padding(8.dp)
                    .padding(start = 16.dp),
                    horizontalArrangement = Arrangement.spacedBy(8.dp)
                    //need to adjust so when you add it drink it will display in the nav bar
                    //for now this works
                    //possibly pull from database instance??
                ) {
                    // Icon(Icons.Filled.Star, contentDescription = "")
                    val list = listOf(
                        "Cranberry Juice",
                        "Rank: 10\n",

                        "Apple Juice",
                        "Rank: 1\n",

                        "PainKiller",
                        "Rank: 6\n",

                        "Menage a Trois",
                        "Rank: 8\n",

                        )
                    val paragraphStyle = ParagraphStyle(textIndent = TextIndent(restLine = 12.sp))
                    Text(
                        buildAnnotatedString {
                            list.forEach {
                                withStyle(style = paragraphStyle) {
                                    append(kotlin.text.Typography.bullet)
                                    append("\t\t")
                                    append(it)
                                }
                            }
                        }
                    )//end text

                    Spacer(modifier = Modifier.padding(30.dp))



                }

            }


        }
        }//end content
    )//en scaffold

}

