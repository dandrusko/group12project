package com.example.finalproject

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.runtime.Composable

import androidx.compose.runtime.CompositionLocalProvider
import com.example.finalproject.ViewModels.UserViewModel
import com.example.finalproject.ViewModels.user

//created a new page

//the problem starts with, why is this signup page not being accepted?
//import com.example.finalproject.ui.theme.SignUpPage

//local provide helps keep track where the user is in the sign on process


class MainActivity : ComponentActivity() {
    private val userState by viewModels<UserViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CompositionLocalProvider(user provides userState) {
                SwitchPages()
            }
        }
    }
}
//this helps to switch from the login page to the main screen/account page or signup page

@Composable
fun SwitchPages() {
    val vm = user.current
    if (vm.loggedIn) {
        MainScreen()
    }
    else if (vm.signUp) {
       //if not logged in, or if not signing up, go to login page
       SignUp()
   }
//this is very naive, but it'll work.. for now
    else if (vm.RecordDrinks){
        RecordDrinks()
    }

    else if (vm.SelectLocation){
        SelectLocation()
    }
        else if (vm.FriendsFav){
            FriendsFav()
    }
        else if(vm.Logout){
            LoginPage()
    }
        else if (vm.back){
            MainScreen()
    }
/**

sorrryyyyy i cut this cuz no worky /:

    else if (vm.favoriteDrinks){
     favoriteDrinks()
    }
    **/

    else {
        LoginPage()
    }
}



