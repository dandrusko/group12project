package com.example.finalproject

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import com.example.finalproject.MainActivity

class CreateNotification(var city: String){
    lateinit var context: Context
    lateinit var notificationbuilder: NotificationCompat.Builder
    val notificationManager: NotificationManager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
    val CHANNEL_ID = "com.example.finalproject"

    init{
        createNotificationChannel()
    }

    fun createNotification() {
        val intent = Intent(context.applicationContext, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(context.applicationContext, 0, intent, PendingIntent.FLAG_IMMUTABLE)
        notificationbuilder = NotificationCompat.Builder(context.applicationContext, CHANNEL_ID)
            .setContentTitle("DrinkApp")
            .setContentText("You are currently in $city")
            .setSmallIcon(androidx.core.R.drawable.notification_template_icon_bg)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "DrinkApp notification"
            val descriptionText = "Notification channel for DrinkApp"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel(CHANNEL_ID, name, importance)
            mChannel.description = descriptionText
            notificationManager.createNotificationChannel(mChannel)
        }
    }

}



