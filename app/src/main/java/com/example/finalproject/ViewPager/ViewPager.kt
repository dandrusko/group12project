package com.example.finalproject.ViewPager

import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.finalproject.R
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState
//the implementation for the viewpager
@ExperimentalPagerApi
@Composable
    fun ViewPager() {

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("Group 12") },
                backgroundColor = MaterialTheme.colors.surface,
            )
        },
        modifier = Modifier.fillMaxSize()
    ) { padding ->
        Column(
            Modifier
                .fillMaxSize()
                .padding(padding)) {
            val pagerState = rememberPagerState(pageCount = 5)

            // Display 10 items
            HorizontalPager(
                 //pageCount = 5,
                state = pagerState,

                modifier = Modifier
                    .padding(horizontal = 32.dp)
                    .weight(1f)
                    .fillMaxWidth(),

                )  { page ->  PagerItem(
                page = page,
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(1f)
            ) }
            }


        }
    }
