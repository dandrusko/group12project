package com.example.finalproject.Api

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

//api model that helps get the information
class ApiModel : ViewModel() {
    private val _friendList = mutableStateListOf<Friends>()
    var errorMessage: String by mutableStateOf("")
    val userList: List<Friends>
        get() = _friendList

    fun getFriendList() {
        viewModelScope.launch {
            val apiService = APIService.getInstance()
            try {
                _friendList.clear()
                _friendList.addAll(apiService.getFriends())

            } catch (e: Exception) {
                errorMessage = e.message.toString()
            }
        }
    }
}