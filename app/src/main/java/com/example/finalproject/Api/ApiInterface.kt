package com.example.finalproject.Api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

data class Friends(
    var id: Int,
    var last: String,
    var drink: String,
    var first: String,
    var avatar: String

)
//the interface that makes the intail call and then stores the information
const val BASE_URL = "https://my-json-server.typicode.com/dandrusko04/users/"

interface APIService {
    @GET("users")
    suspend fun getFriends(): List<Friends>

    companion object {
        var apiService: APIService? = null
        fun getInstance(): APIService {
            if (apiService == null) {
                apiService = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(APIService::class.java)
            }
            return apiService!!
        }
    }
}